module gitlab.com/philcms1/go-sample-data-server

go 1.21.0

require (
	github.com/joho/godotenv v1.5.1
	go.uber.org/zap v1.25.0
)

require go.uber.org/multierr v1.10.0 // indirect
