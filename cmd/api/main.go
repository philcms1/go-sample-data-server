package main

import (
	"encoding/json"
	"flag"
	"gitlab.com/philcms1/go-sample-data-server/internal"
	"go.uber.org/zap"
	"log"
	"net/http"
	"strconv"
	"time"
)

type application struct {
	config *internal.Config
	logger *zap.SugaredLogger
}

// TODO: inject version from CI build
const version = "1.0.0"

func main() {
	var cfg *internal.Config
	var env = flag.String("env", "development", "Environment to use (development|staging|production)")
	flag.Parse()
	log.Printf("env: %s\n", *env)

	cfg = internal.GetConfig(*env)
	var portStr = strconv.Itoa(cfg.ServerConfig.Port)

	logger := initializeLogger()
	if logger == nil {
		panic("logger is nil")
		return
	}

	app := &application{
		config: cfg,
		logger: initializeLogger(),
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/v1/healthcheck", app.healthcheckHandler)

	srv := &http.Server{
		Addr:         ":" + portStr,
		Handler:      mux,
		IdleTimeout:  time.Minute,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	// Start HTTP server
	app.logger.Infof("Starting %s server on port %s", *env, portStr)
	err := srv.ListenAndServe()
	if err != nil {
		app.logger.Fatal(err)
	}
}

func initializeLogger() *zap.SugaredLogger {
	rawJSON := []byte(`{
	  "level": "debug",
	  "encoding": "console",
	  "outputPaths": ["stdout"],
	  "errorOutputPaths": ["stderr"],
	  "encoderConfig": {
	    "messageKey": "message",
	    "levelEncoder": "lowercase"
	  }
	}`)
	var cfg zap.Config
	if err := json.Unmarshal(rawJSON, &cfg); err != nil {
		panic(err)
	}
	logger, err := cfg.Build()
	if err != nil {
		panic(err)
	}
	return logger.Sugar()
}
