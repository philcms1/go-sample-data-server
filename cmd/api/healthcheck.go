package main

import (
	"fmt"
	"net/http"
)

func (app *application) healthcheckHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "I'm alive!")
	fmt.Fprintf(w, "environment: %s\n", app.config.ServerConfig.Env)
	fmt.Fprintf(w, "version: %s\n", version)
}
